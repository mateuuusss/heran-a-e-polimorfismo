#include "triangulo.hpp"

Triangulo::Triangulo (){
    set_tipo("Triangulo triangular");
    set_base(2.0f);
    set_altura(3.0f);

}

Triangulo::Triangulo(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);

}

Triangulo::~Triangulo(){
    cout << "destruindo o triangulo" << endl;
}

float Triangulo::calcula_area(){
    return ((get_base() * get_altura())/2);
}
float Triangulo::calcula_perimetro(){
    return ((get_base() * 2) + get_altura() * 2);
}
