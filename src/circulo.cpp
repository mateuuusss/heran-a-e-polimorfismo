#include "formageometrica.hpp"
#include "circulo.hpp"
#include <math.h>

Circulo::Circulo(){
    set_tipo("Circulo redondo");
    raio = 2.5f;

}

Circulo::Circulo(string tipo, float raio){
    set_tipo(tipo);
    this->raio= raio;    
}

Circulo::~Circulo(){
    cout << "destruindo o circulo" << endl;
}

float Circulo::calcula_area(){
    return (3.14*pow(raio,2));
}

float Circulo::calcula_perimetro(){
    return (2*3.14*raio);
}