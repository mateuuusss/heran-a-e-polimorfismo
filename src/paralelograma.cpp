#include "paralelograma.hpp"
#include <iostream>
using namespace std;

Paralelograma::Paralelograma (){
    set_tipo("Paralelograma retangular inclinado\n");
    set_base(5.0f);
    set_altura(2.0f);

}

Paralelograma::Paralelograma (string tipo, float base, float altura){
    set_tipo (tipo);
    set_base (base);
    set_altura (altura);
}

Paralelograma::~Paralelograma (){
    cout << "destruindo o quadrado" << endl;
}
