#include "quadrado.hpp"
#include <iostream>

using namespace std;

Quadrado::Quadrado (){
    set_tipo("Quadrado quadrangular");
    set_base(2.0f);
    set_altura(2.0f);

}

Quadrado::Quadrado(string tipo, float base, float altura){
    set_tipo(tipo);
    if (base != altura){
        throw(1);
    }
    else{
        set_base(base);
        set_altura(altura);
    }

}

Quadrado::~Quadrado(){
    cout << "destruindo o quadrado" << endl;
}
