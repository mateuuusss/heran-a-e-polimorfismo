#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include "formageometrica.hpp"

class Pentagono : public FormaGeometrica{
public:
    Pentagono ();
    Pentagono (string tipo, float base, float altura);
    ~Pentagono();
    float calcula_area();
    float calcula_perimetro();
};



#endif 